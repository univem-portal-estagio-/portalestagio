﻿using PortalEstagio.Domain.PortalAluno.Entities;
using PortalEstagio.Domain.PortalAluno.Repositories;
using PortalEstagio.Infra.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalEstagio.Infra.Repositories
{
    public class AlunoRepository : IAlunoRepository
    {
        private readonly PortalAlunoDataContext _context;
        public AlunoRepository(PortalAlunoDataContext context)
        {
            _context = context;
        }
        public IEnumerable<AlunoEntity> GetAlunos()
        {
            return _context.Alunos.ToList();
        }

        public AlunoEntity GetById(Guid id)
        {
            return _context.Alunos.AsNoTracking().FirstOrDefault(x => x.Id == id);
        }

        public void Save(AlunoEntity aluno)
        {
             _context.Alunos.Add(aluno);
            _context.SaveChanges();
        }
    }
}
