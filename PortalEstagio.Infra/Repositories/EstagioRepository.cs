﻿using PortalEstagio.Domain.PortalAluno.Entities;
using PortalEstagio.Domain.PortalAluno.Repositories;
using PortalEstagio.Infra.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalEstagio.Infra.Repositories
{
    public class EstagioRepository : IEstagioRepository
    {
        private readonly PortalAlunoDataContext _context;

        public EstagioRepository(PortalAlunoDataContext context)
        {
            _context = context;
        }
        public void cadastrarEstagio(EstagioEntity Estagio)
        {
            _context.Estagios.Add(Estagio);
            _context.SaveChanges();
        }

        public void DeletarEstagio(Guid id)
        {
            throw new NotImplementedException();
        }

        public void EditarEstagio(Guid EstagioId)
        {
           // _context.Estagios.
        }

        public IEnumerable<EstagioEntity> GetEstagioByAluno(Guid alunoId)
        {
            throw new NotImplementedException();
            //return _context.Estagios.AsNoTracking().FirstOrDefault(x => x.Aluno.Id == alunoId);
        }

        public EstagioEntity GetEstagioById(Guid estagioId)
        {
            return _context.Estagios.AsNoTracking().FirstOrDefault(x => x.Id == estagioId);
        }

        public IEnumerable<EstagioEntity> GetEstagios()
        {
            return _context.Estagios.ToList();
        }

        public void inativarEstagio(Guid estagioId)
        {
           // _context.Estagios.AsNoTracking().FirstOrDefault(x => x.Id == estagioId);
        }
    }
}
