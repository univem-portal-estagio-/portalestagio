namespace PortalEstagio.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AlunoEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nome = c.String(nullable: false),
                        Ra_codigo = c.String(nullable: false, maxLength: 6),
                        Curso = c.String(),
                        Cpf = c.String(maxLength: 11),
                        Etapa = c.String(),
                        Data_nasc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EstagioEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DataInicio = c.DateTime(nullable: false),
                        DataFim = c.DateTime(nullable: false),
                        BolsaAuxilio = c.Decimal(nullable: false, storeType: "money"),
                        AuxilioTransporte = c.Decimal(nullable: false, storeType: "money"),
                        Status = c.Boolean(nullable: false),
                        Empresa = c.String(nullable: false),
                        Intermediador = c.String(nullable: false),
                        Aluno_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AlunoEntities", t => t.Aluno_Id, cascadeDelete: true)
                .Index(t => t.Aluno_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EstagioEntities", "Aluno_Id", "dbo.AlunoEntities");
            DropIndex("dbo.EstagioEntities", new[] { "Aluno_Id" });
            DropTable("dbo.EstagioEntities");
            DropTable("dbo.AlunoEntities");
        }
    }
}
