﻿using PortalEstagio.Domain.PortalAluno.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalEstagio.Infra
{
    public class EstagioMap : EntityTypeConfiguration<EstagioEntity>
    {
        public EstagioMap()
        {
            HasKey(x => x.Id);
            Property(x => x.Empresa).IsRequired();
            Property(x => x.Intermediador).IsRequired();
            Property(x => x.DataInicio);
            Property(x => x.DataFim);
            Property(x => x.BolsaAuxilio).HasColumnType("money"); 
            Property(x => x.AuxilioTransporte).HasColumnType("money");
            HasRequired(x => x.Aluno);

            //comandos: Enable-Migrations
            // Add-Migration V1
            //Update-Database
        }
    }
}
