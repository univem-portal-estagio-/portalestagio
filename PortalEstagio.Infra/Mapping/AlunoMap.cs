﻿using PortalEstagio.Domain.PortalAluno.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalEstagio.Infra
{
    public class AlunoMap : EntityTypeConfiguration<AlunoEntity>
    {
        public AlunoMap()
        {
            HasKey(x => x.Id);
            Property(x => x.Nome).IsRequired();
            Property(x => x.Ra.codigo).IsRequired().HasMaxLength(6);
            Property(x => x.Cpf).HasMaxLength(11);
            Property(x => x.Curso);
            Property(x => x.Etapa);
            Property(x => x.Data_nasc);
        }
    }
}
