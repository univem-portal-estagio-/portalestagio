﻿using PortalEstagio.Domain.PortalAluno.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalEstagio.Infra.Contexts
{
    public class PortalAlunoDataContext : DbContext
    {
        public PortalAlunoDataContext() : base(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=PortalEstagioDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False")
        {
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<AlunoEntity> Alunos { get; set; }
        public DbSet<EstagioEntity> Estagios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AlunoMap());
            modelBuilder.Configurations.Add(new EstagioMap());
        }
    }
}

