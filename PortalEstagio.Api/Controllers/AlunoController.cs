﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PortalEstagio.Domain.PortalAluno.Entities;
using PortalEstagio.Domain.PortalAluno.Repositories;
using PortalEstagio.Infra.Transactions;

namespace PortalEstagio.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlunoController : ControllerBase
    {
        private readonly IAlunoRepository _repository;
        private readonly IUow _uow;
        public AlunoController(IAlunoRepository repository, IUow uow)
        {
            _repository = repository;
            _uow = uow;
        }
        // GET: api/Aluno
        [HttpGet]
        public IEnumerable<AlunoEntity> Get()
        {
            return _repository.GetAlunos();
        }

        // GET: api/Aluno/5
        [HttpGet("{id}")]
        public AlunoEntity Get(Guid id)
        {
            return _repository.GetById(id);
        }

        // POST: api/Aluno
        [HttpPost]
        public void Post([FromBody] AlunoEntity value)
        {
            _repository.Save(value);
            _uow.Commit();
        }

        // PUT: api/Aluno/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
