﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PortalEstagio.Domain.PortalAluno.Entities;
using PortalEstagio.Domain.PortalAluno.Repositories;
using PortalEstagio.Infra.Transactions;

namespace PortalEstagio.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstagioController : ControllerBase
    {
        private readonly IEstagioRepository _repository;
        private readonly IUow _uow;

        public EstagioController(IEstagioRepository repository, IUow uow)
        {
            _repository = repository;
            _uow = uow;
        }


        // GET: api/Estagio
        [HttpGet]
        public IEnumerable<EstagioEntity> Get()
        {
            return _repository.GetEstagios();
        }

        // GET: api/Estagio/5
        [HttpGet("{id}")]
        public EstagioEntity Get(Guid id)
        {
            return _repository.GetEstagioById(id);
        }

        // POST: api/Estagio
        [HttpPost]
        public void Post([FromBody] EstagioEntity value)
        {
            _repository.cadastrarEstagio(value);
            _uow.Commit();
        }

        // PUT: api/Estagio/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody] string value)
        {
            _repository.EditarEstagio(id);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _repository.DeletarEstagio(id);
        }
    }
}
