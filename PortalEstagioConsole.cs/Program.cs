﻿using System;
using System.Collections.Generic;
using PortalEstagio.Domain.PortalAluno.Entities;
using PortalEstagio.Domain.PortalAluno.Repositories;
using PortalEstagio.Domain.PortalAluno.ValueObjects;

namespace PortalEstagio.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Para gerar um Estagio necessita-se de um Aluno
           

            var fakeAlunoRepository = new FakeAlunoRepository(); //aluno falso
            var fakeEstagioRepository = new FakeEstagioRepository(); //estagio falso
        
            GenerateEstagio(fakeAlunoRepository, fakeEstagioRepository);

            
            System.Console.ReadKey();
        }


        public static void GenerateEstagio(
            IAlunoRepository alunoRepository,
            IEstagioRepository estagioRepository)
        {
            Guid alunoID = Guid.NewGuid(); //ID falso
            Guid estagioID = Guid.NewGuid(); //ID falso

            var aluno = alunoRepository.GetById(alunoID); //recuperando aluno falso
            var estagio = estagioRepository.GetEstagioById(estagioID);// recuperando estagio falso

            //estagio.SetAluno(aluno);

        }
    }

    public class FakeEstagioRepository : IEstagioRepository
    {
        public void cadastrarEstagio(EstagioEntity EstagioEntity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EstagioEntity> GetEstagioByAluno(Guid alunoId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EstagioEntity> GetEstagios()
        {
            throw new NotImplementedException();
        }

        public EstagioEntity GetEstagioById(Guid estagioId)
        {
            var EstagioFake = new EstagioEntity
            (
                "marilan",
                "nuppe",
                new DateTime(2019, 04, 15),
                new DateTime(2020, 04, 15),
                850,
                100,
                true,
                new AlunoEntity("Aluno Falso",
                new Ra("123456"),
                "1234567890",
                "Computação",
                "5",
                "noturno",
                new DateTime(1999, 04, 15)
                )
            );
            return EstagioFake;
        }

        public void inativarEstagio(Guid estagioId)
        {
            throw new NotImplementedException();
        }

        public void EditarEstagio(Guid EstagioId)
        {
            throw new NotImplementedException();
        }

        public void DeletarEstagio(Guid id)
        {
            throw new NotImplementedException();
        }
    }

    public class FakeAlunoRepository : IAlunoRepository
    {

        //Vamos criar um Customer Fake para fazermos testes
        public AlunoEntity GetById(Guid id)
        {
            var AlunoFake = new AlunoEntity
            (
                "Aluno Falso",
                new Ra("123456"),
                "1234567890",
                "Computação",
                "5",
                "noturno",
                new DateTime(1999,04,15)
                
            );
            return AlunoFake;
        }

        public void Save(AlunoEntity customer)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AlunoEntity> GetAlunos()
        {
            throw new NotImplementedException();
        }
    }
}
