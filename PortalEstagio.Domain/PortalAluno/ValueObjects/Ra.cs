﻿using PortalEstagio.Shared.ValueObjects;
using PortalEstagio.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalEstagio.Domain.PortalAluno.ValueObjects
{
    public class Ra : ValueObject
    {
        public Ra()
        {

        }
        public Ra(string _ra)
        {
            if(_ra.Length==6 && IsDigitsOnly(_ra))
            {
                codigo = _ra;
            }
            
        }

        public string codigo { get; set; }

        bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }
    
}
