﻿using PortalEstagio.Domain.PortalAluno.ValueObjects;
using PortalEstagio.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalEstagio.Domain.PortalAluno.Entities
{
    public class AlunoEntity : Entity
    {
        protected AlunoEntity()
        {
                
        }
        
        public AlunoEntity(
            string nome,
            Ra ra,
            string cpf,
            string curso,
            string etapa,
            string turno,
            DateTime data_nasc)
        {
            Nome = nome;
            Ra = ra;
            Cpf = cpf;
            Curso = curso;
            Etapa = etapa;
            Data_nasc = data_nasc;
        }

        public string Nome { get; private set; }
        public Ra Ra { get; private set; }
        public string Curso { get; private set; }
        public string Cpf { get; private set; }
        public string Etapa { get; private set; }
        public DateTime Data_nasc { get; set; }

    }
}
