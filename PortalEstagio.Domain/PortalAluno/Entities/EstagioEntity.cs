﻿using PortalEstagio.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PortalEstagio.Domain.PortalAluno.Entities
{
    public class EstagioEntity : Entity
    {
        protected EstagioEntity()
        {

        }
        public EstagioEntity(
            string empresa,
            string intermediador,
            DateTime dataInicio,
            DateTime dataFim,
            decimal bolsaAuxilio,
            decimal auxilioTransporte,
            bool status,
            AlunoEntity aluno
            )
        {
            Empresa = empresa;
            Intermediador = intermediador;
            DataInicio = dataInicio;
            DataFim = dataFim;
            BolsaAuxilio = bolsaAuxilio;
            AuxilioTransporte = auxilioTransporte;
            Status = status;
            Aluno = aluno;
        }

       



        public DateTime DataInicio { get; private set; }
        public DateTime DataFim { get; private set; }
        public decimal BolsaAuxilio { get; private set; }
        public decimal AuxilioTransporte { get; private set; }
        public bool Status { get; private set; }
        public AlunoEntity Aluno { get; private set; }
        public string Empresa { get; private set; }
        public string Intermediador { get; private set; }
    }

    
}
