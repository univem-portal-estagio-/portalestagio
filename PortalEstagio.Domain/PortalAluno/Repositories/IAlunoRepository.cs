﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortalEstagio.Domain.PortalAluno.Entities;


namespace PortalEstagio.Domain.PortalAluno.Repositories
{
    public interface IAlunoRepository
    {
        AlunoEntity GetById(Guid id);
        void Save(AlunoEntity aluno);
        IEnumerable<AlunoEntity> GetAlunos();

    }
}
