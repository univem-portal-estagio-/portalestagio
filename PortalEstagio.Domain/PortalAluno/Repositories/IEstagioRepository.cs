﻿

using PortalEstagio.Domain.PortalAluno.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalEstagio.Domain.PortalAluno.Repositories
{
    public interface IEstagioRepository
    {
        void cadastrarEstagio(EstagioEntity Estagio);
        void inativarEstagio(Guid estagioId);
        void EditarEstagio(Guid EstagioId);
        EstagioEntity GetEstagioById(Guid estagioId);
        IEnumerable<EstagioEntity> GetEstagios();
        IEnumerable<EstagioEntity> GetEstagioByAluno(Guid alunoId);
        void DeletarEstagio(Guid id);
    }
}
